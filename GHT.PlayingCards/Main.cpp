
// Test comment to practice push

#include <iostream>;
#include <conio.h>;

using namespace std;

//enums for rank and suit
enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Spades,
	Hearts,
	Clubs,
	Diamonds
};

//struct for a card
struct Card
{
	Rank rank = Two;
	Suit suit = Spades;
};

void PrintCard(Card c)
{
	cout << "The ";
	
	switch (c.rank) // checks the rank converting it into their card rank
	{
	case 2: cout << "Two"; break;
	case 3: cout << "Three"; break;
	case 4: cout << "Four"; break;
	case 5: cout << "Five"; break;
	case 6: cout << "Six"; break;
	case 7: cout << "Seven"; break;
	case 8: cout << "Eight"; break;
	case 9: cout << "Nine"; break;
	case 10: cout << "Ten"; break;
	case 11: cout << "Jack"; break;
	case 12: cout << "Queen"; break;
	case 13: cout << "King"; break;
	case 14: cout << "Ace"; break;

	default:
		break;
	}
	cout << " of ";
	switch (c.suit) // converting the card into its suit
	{
	case 0: cout << "Spades"; break;
	case 1: cout << "Hearts"; break;
	case 2: cout << "Clubs"; break;
	case 3: cout << "Diamonds"; break;

	default:
		break;
	}
	cout << "\n\n";

	 //cout << "The " << c.rank << " of " << c.suit << "\n";
}

Card HighCard(Card card1, Card card2) // checks to see what the highcard is
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		return card2;
	}
	else if (card1.rank == card2.rank)
	{
		if (card1.suit > card2.suit)
		{
			return card1;
		}
		else if (card1.suit < card2.suit)
		{
			return card2;
		}
	}
}

int main()
{

	Card a;
	a.rank = Jack;
	a.suit = Hearts;

	Card b;
	b.rank = Ace;
	b.suit = Spades;
	PrintCard(b);

	PrintCard(HighCard(a, b)); // prints and checks what card is higher

	(void)_getch();
	return 0;
}